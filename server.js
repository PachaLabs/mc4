require('dotenv').config();
var express = require('express')
var app = express()
var server = require('http').Server(app)
var io = require('socket.io')(server)
var path = require('path')
var webpack = require('webpack')
var webpackMiddleware = require('webpack-dev-middleware')
var webpackHotMiddleware = require('webpack-hot-middleware')
var config = require('./webpack.config.js')
var compiler = webpack(config);
var stormpath = require('express-stormpath')
var bodyParser = require('body-parser')
var request = require('request')
var MongoClient = require('mongodb').MongoClient
var moment = require('moment')

app.set('port', (process.env.PORT || 8080))
app.use(express.static(__dirname + '/app'))
app.use(webpackMiddleware(compiler))
app.use(webpackHotMiddleware(compiler))
app.use(stormpath.init(app, {
	web: {
		spa: {
			enabled: true,
			view: path.join(__dirname, 'app', 'index.html')
		},
		me: {
			expand: {
				customData: true
			}
		},
		register: {
			form: {
				fields: {
					givenName: {
						enabled: false
				 	},
					surname: {
						enabled: false
				    }
				}
			}
		}
	},
	expand: {
		customData: true
	},
	postRegistrationHandler: function (account, req, res, next) {
	  	userIdSplit = account.href.split('/')
	  	stormpathId = userIdSplit[userIdSplit.length - 1]
	  	req.user.customData.userId = stormpathId
	  	req.user.customData.save()
		DbConnection().then(db => {
			db.collection('users').insert({
		  		stormpathId: stormpathId,
		  		email: account.email,
		  		menu: [],
		  		settings: {
		  			publish: false,
		  			name: '',
		  			phone: '',
		  			address: '',
		  			city: '',
		  			state: '',
		  			notifications: true,
		  			cashPayments: false,
		  			dateRegistered: moment().format(),
		  			noFee: false,
		  			hours: [
		  				{
		  					day: 'Monday',
		  					hoursAm: '10:00',
		  					hoursPm: '21:00'
		  				},
		  				{
		  					day: 'Tuesday',
		  					hoursAm: '10:00',
		  					hoursPm: '21:00'
		  				},
		  				{
		  					day: 'Wednesday',
		  					hoursAm: '10:00',
		  					hoursPm: '21:00'
		  				},
		  				{
		  					day: 'Thursday',
		  					hoursAm: '10:00',
		  					hoursPm: '21:00'
		  				},
		  				{
		  					day: 'Friday',
		  					hoursAm: '10:00',
		  					hoursPm: '21:00'
		  				},
		  				{
		  					day: 'Saturday',
		  					hoursAm: '10:00',
		  					hoursPm: '21:00'
		  				},
		  				{
		  					day: 'Sunday',
		  					hoursAm: '10:00',
		  					hoursPm: '21:00'
		  				}
		  			],
		  			delivery: false,
		  			deliveryOptions: {
			        	polygon: [],
			            deliveryMin: 10,
			            deliveryFee: 0
		  			}
		  		},
		  		orders: [],
			    stripe: {
			        access_token: '',
			        livemode: false,
			        refresh_token: '',
			        token_type: '',
			        stripe_publishable_key: '',
			        stripe_user_id: '',
			        scope: ''
			    }
			});
		});
	    console.log(account);
	    next();
	}
}));

app.on('stormpath.ready', function () {	
	console.log(stormpath);
	server.listen(app.get('port'), function() {
	    console.log('Listening to', app.get('port'))
	})
});

function DbConnection() {
	return new Promise((resolve, reject) => {
		MongoClient.connect("mongodb://" + process.env.DB_USER + ":" + process.env.DB_PASSWORD + "@ds017862.mlab.com:17862/menucamp", 
			(err, db) => {
 				err ? reject(err) : resolve(db);
			}
		);
	});
};

app.post('/stripeReturn', function (req, res) {
	console.log(res)
})

io.on('connection', function (socket) {
	socket.on('requestData', function (user, callback) {
		console.log('hi')
		DbConnection().then(db => {
			db.collection('users').findOne({stormpathId: user.customData.userId}, function(err, userObject) {
					console.log(err, userObject)
					callback(err, userObject)
				})
		})
	});
	socket.on('requestMenu', function (restaurantId, callback) {
		DbConnection().then(db => {
			db.collection('users').findOne({stormpathId: restaurantId}, function(err, userObject) {
					callback(err, userObject)
				})
		})
	});
	socket.on('requestMenuLive', function (restaurantId, callback) {
		DbConnection().then(db => {
			db.collection('users').findOne({stormpathId: restaurantId}, function(err, userObject) {
					sendObject = {
						items: userObject.menu,
						settings: userObject.settings,
						deliveryEnabled: userObject.settings.delivery,
						deliveryOptions: userObject.settings.deliveryOptions,
						stripeEnabled: userObject.stripe.stripe_user_id,
						publishMenu: userObject.settings.publish
					}
					callback(err, sendObject)
				})
		})
	});
	socket.on('mongoUpdate', function (user, query, updateData) {
		DbConnection().then(db => {
			var newDat = {}
			newDat[query] = updateData
			db.collection('users').update({stormpathId: user.customData.userId}, {$set: newDat });
		});
	});
	socket.on('mongoUpdateObject', function (user, updateData) {
		DbConnection().then(db => {
			db.collection('users').update({stormpathId: user.customData.userId}, {$set: updateData });
		});
	});
	socket.on('mongoNewOrders', function (user, updateData) {
		console.log(user)
		DbConnection().then(db => {
			db.collection('users').update({stormpathId: user}, {$push: {orders: updateData}})
		})
	})

	socket.on('email', function (emailFrom, emailTo, emailSubject, emailText, callback) {
		request.post(
			{
				url: 'https://api.mailgun.net/v3/menucamp.com/messages', 
				auth: {
					user: 'api',
					password: process.env.MAILGUN_PASSWORD
				},
				form: {
					from: emailFrom,
					to: emailTo,
					subject: emailSubject,
					text: emailText
				}
			},
			function (err, httpResponse, body) {
				console.log(err);
				console.log(body);
				callback(err, body)
			})
	});

	//after user connects with stripe they are rerouted to site with code, submit code to recieve stripe auth token 
	socket.on('stripeToken', function (stripeAuthorization, callback) {
		request.post(
			{
				url: 'https://connect.stripe.com/oauth/token',
				form: {
					client_secret: process.env.STRIPE_CLIENT_SECRET,
					code: stripeAuthorization,
					grant_type: 'authorization_code'
				}
			},
			function (err, httpResponse, body) {
				console.log(err);
				console.log(body);
				console.log(httpResponse.data);
				callback(err, body)
			}
		)
	});
	socket.on('stripeCharge', function (token, user, orderIndex, stripeAccount, item) {
		// Check if account has free application fee
		applicationFee = 0
		console.log(moment(user.dateRegistered).isAfter(moment().subtract(2, 'months')))
		if (moment(user.dateRegistered).isAfter(moment().subtract(2, 'months'))) {
			applicationFee = 0
		} else {
			applicationFee = parseInt((item.total * 100) * 0.02)
		}

		request.post(
			{
				url: 'https://api.stripe.com/v1/charges',
				auth: {
					'bearer': process.env.STRIPE_CLIENT_SECRET
				},
				form: {
					destination: stripeAccount,
					source: token,
					amount: parseInt(item.total * 100),
					currency: 'usd',
					application_fee: applicationFee
				}
			},
			function (err, httpResponse, body) {
				console.log(err);
				console.log(body);
				socket.emit('mongoUpdateOrder', user, 'actionTaken', orderIndex, err ? false : true)
				DbConnection().then(db => {
					db.collection('users').update({'orders.token.id': token}, {'$set': {'orders.$.actionTaken':  err ? false : true}})
				})
			}
		)
	});
	socket.on('stripeCardToken', function () {
		request.post(
			{
				url: 'https://api.stripe.com/v1/tokens',
				auth: {
					'bearer': process.env.STRIPE_CLIENT_SECRET
				},
				form: {
					card: {
						exp_month: 10,
						exp_year: 2018,
						number: 4242424242424242,
						cvc: 123
					}
				}
			},
			function (err, httpResponse, body) {
				console.log(err);
				console.log(body)
			}
		)
	});
	
	socket.on('addressCheck', function (address, callback) {
		var geoCodeKey = process.env.GOOGLE_GEOCODE_KEY;
		var addressRequestURL = 'https://maps.googleapis.com/maps/api/geocode/json?address='+ address + '&key=' + geoCodeKey;
		request.get({url: addressRequestURL}, function (error, response, body) {
			console.log(error);
			console.log(body);
			callback(error, body)
		});
	});
})