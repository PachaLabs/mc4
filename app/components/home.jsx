import React from 'react'

import HomeBar from './homeBar.jsx'

import mainPic from './blueflame.jpg'
import exampleMenu from './exampleMenu.png'

import { LoginRoute, LogoutLink } from 'react-stormpath';

class Home extends React.Component {
	render () {

		return (
			<div className='allHome'>
				<HomeBar />

				<div className="mainContainerBorderless">
					<img src={mainPic} className="headerPic"/>
				</div>

				<div className="mainContainerBorderless">
					<div className="centerTitle">Online ordering made easy</div>
					<div className="centerParagraph">Set up a menu for free and start accepting orders in minutes.</div>
				</div>

				<div className="mainContainer">
					<div className="mcTitle">
						<div className="mcTitleInner">Pricing</div>
					</div>
					<div className="mcBody">
						<div className="spaceRow"></div>
						<div className="centerTitle">$0 monthly fees</div>
						<div className="centerTitle">+</div>
						<div className="centerTitle">2% per transaction</div>
						<div className="centerTitle">+</div>
						<div className="centerTitle">Credit card processing</div>
						<div className="spaceRow"></div>
					</div>
				</div>

				<div className="mainContainerBorderless">
					<img src={exampleMenu} className="halfPic"/>
					<div className="halfPicCaption">
						<div className="leftTitle captionDownShift">See an Example Menu</div>
						<div>Add our ordering system to new or existing websites with a simple link:</div>
						<div className="spaceRow"></div>
						<a className="blueButton" href="/#/menu/1Xco8y5px5XOgb4TJh1C1V">Example</a>
					</div>
				</div>

				<div className="mainContainer">
					<div className="mcTitle">
						<div className="mcTitleInner">How it Works</div>
					</div>
					<div className="spaceRow"></div>
					<div className="centerTitle">1. Sign up with us and Stripe</div>
					<div className="spaceRow"></div>
					<div className="centerTitle">2. Input your menu</div>
					<div className="spaceRow"></div>
					<div className="centerTitle">3. Customize your settings</div>
					<div className="spaceRow"></div>
					<div className="centerTitle">4. Start accepting orders</div>
					<div className="spaceRow"></div>
				</div>

				<div className="footer"></div>
			</div>
		)
	}
}

export default Home