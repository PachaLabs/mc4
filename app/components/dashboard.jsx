import React from 'react'
import moment from 'moment'
import io from 'socket.io-client'
var socket = io()

import TopBar from './topBar.jsx'

class OrderLine extends React.Component	{
	static contextTypes = {
		user: React.PropTypes.object
	}
	constructor(props) {
		super(props)
		this.state = {
			newCheck: !this.props.order.actionTaken,
			collapsed: true
		}
	}

	handleClick = () => {
		let toggleCollapse = !this.state.collapsed
		this.setState({
			collapsed: toggleCollapse
		})
	}
	acceptOrder = () => {
		console.log(this.props)
		socket.emit('stripeCharge', this.props.order.token.id, this.context.user, this.props.orderIndex, this.props.stripeAccount, this.props.order)
	}
	render () {
		let isItNew = this.state.newCheck ? 'newCircle': 'newCircle notNew'
		let isItCollapsed = this.state.collapsed ? 'orderDetailsHidden': 'orderDetailsShow'

		let cartList = this.props.order.cart.map((item, i) => {
			return (
				<div key={i} className="cartListOuter">
					<div className="detailLine">{item.quantity} x {item.item.title}</div>
					<div className="detailLine">{item.instructions}</div>
				</div>
			)
		})

		return (
				<div>
					
					<div className="orderBar" onClick={() => this.handleClick()}>
						<div className="orderHeader">
							<div className={isItNew}></div>
							<div className="orderTitle">
								${this.props.order.total} : {this.props.order.date}
							</div>
						</div>
						<div className={isItCollapsed}>
							<div className="detailLine">Email: {this.props.order.token.email}</div>
							<div className="detailLine">Phone: </div>
							<div className="detailLine">Delivery: {this.props.order.deliveryEnabled ? 'true' : 'false'}</div>
							<div className="detailLine">Cart: </div>
							{cartList}
							<div className="spaceRow"></div>
							<div className={this.props.buttons ? 'detailLine' : 'detailLine hidden'}><div className="blueButton inlineBlock" onClick={this.acceptOrder}>Accept</div><div className="whiteButton inlineBlock">Reject</div></div>
						</div>
					</div>
				</div>
		)
	}
}

class NewOrders extends React.Component	{
	constructor(props) {
		super(props)
		this.state = {
			orders: this.props.orders
		}
	}

	render () {

		console.log(this.props.orderList)
		let orderList = this.props.orderList.map((item, i) => {
			if (!item.actionTaken) {
				return (
					<OrderLine key={i} order={item} orderIndex={i} stripeAccount={this.props.stripeAccount} buttons={true} />
				)
			}
		})

		return (
			<div className="mainContainer">
				<div className="mcTitle">
					<div className="mcTitleInner">New Orders</div>
				</div>
				<div className="mcBody">
						{orderList}
				</div>
			</div>
		)
	}
}

class OrderHistory extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			orders: this.props.orders
		}
	}
	render () {

		let orderHistoryList = this.props.orderList.map((item, i) => {
			if (item.actionTaken) {
				return (
					<OrderLine key={i} order={item} buttons={false} />
				)
			}
		})

		return (
			<div className="mainContainer">
				<div className="mcTitle">
					<div className="mcTitleInner">Order History</div>
				</div>
				<div className="mcBody">
						{orderHistoryList}
				</div>
			</div>
		)
	}
}

class Dashboard extends React.Component {
	static contextTypes = {
		user: React.PropTypes.object
	}
	constructor(props) {
		super(props)

		let rightNow = moment().format('MMMM Do YYYY, h:mm:ss a')
		this.state = {
			orderList: [],
			orders: [{
				customer: {
					name: 'Ian E Crisp',
					email: 'crisp@bit.com',
					phone: '4409339215'
				},
				delivery: true,
				actionTaken: false,
				accepted: false,
				total: 890,
				date: rightNow, 
				items: [{
					title: 'kush',
					price: 800,
					qty: 3,
					instructions: 'hold the mayo'
				}]
			},{
				customer: {
					name: 'Ian E Crisp',
					email: 'crisp@bit.com',
					phone: '4409339215'
				},
				delivery: true,
				actionTaken: false,
				accepted: false,
				total: 890,
				date: rightNow, 
				items: [{
					title: 'kush',
					price: 800,
					qty: 3,
					instructions: 'hold the mayo'
				}]
			},{
				customer: {
					name: 'Ian E Crisp',
					email: 'crisp@bit.com',
					phone: '4409339215'
				},
				delivery: true,
				actionTaken: true,
				accepted: false,
				total: 890,
				date: rightNow, 
				items: [{
					title: 'kush',
					price: 800,
					qty: 3,
					instructions: 'hold the mayo'
				}]
			}]
		}
	}

	componentDidMount() {
		socket.emit('requestData', this.context.user, (err, userObject) => {
			this.firstLoad(userObject)
		})
	}
	firstLoad = (userObject) => {
		this.setState({
			orderList: userObject.orders,
			stripeAccount: userObject.stripe.stripe_user_id
		})
	}
	render () {
		
		return (
			<div>
				<TopBar />

				<div className="spaceRow"></div>
				<div className="spaceRow"></div>

				<div className="centerTitle">Your Orders</div>

				<NewOrders orderList={this.state.orderList} orders={this.state.orders} stripeAccount={this.state.stripeAccount} />

				<OrderHistory orderList={this.state.orderList} orders={this.state.orders} />

			</div>
		)
	}
}

export default Dashboard