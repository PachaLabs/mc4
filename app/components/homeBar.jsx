import React from 'react'
import { LoginRoute, LogoutLink } from 'react-stormpath';
import logo from './logo.svg'

class HomeBar extends React.Component {
	static contextTypes = {
		user: React.PropTypes.object
	}
	render () {
		let acctClass
		let logoutClass
		if (this.context.user == null) {
			acctClass = 'accountButton'
			logoutClass ='hidden'
		} else {
			acctClass = 'hidden'
			logoutClass ='accountButton'
		}
		return (
			<div className="mainContainerBorderless topBar">
			    <img src={logo} className="logoPic"/>
			    <a className="logoText" href="/">menucamp</a>
			    <div className="topRight">
				    <a className={acctClass} href="/#/login">Log In</a>
				    <a className={acctClass} href="/#/register">Sign Up</a>
				    <a className={logoutClass} href="/#/dashboard">Account</a>
				    <LogoutLink className={logoutClass}>Logout</LogoutLink>
				</div>
			</div>
		)
	}
}

export default HomeBar