import React from 'react'
import StripeCheckout from 'react-stripe-checkout';
import io from 'socket.io-client'
import moment from 'moment'
var socket = io()

class LiveMenu extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			cart: [],
			subtotal: 0,
			tax: 0,
			taxRate: 1.05,
			precart: [],
			items: [],
			settings: [],
			deliveryEnabled: false,
			deliveryOptions: {}
		}
		socket.emit('requestMenuLive', this.props.params.restaurantId, (err, userObject) => {
			this.firstLoad(userObject)
		})
	}

	firstLoad = (userObject) => {
		this.setState({
			items: userObject.items,
			settings: userObject.settings,
			deliveryEnabled: userObject.deliveryEnabled,
			deliveryOptions: userObject.deliveryOptions,
			stripeEnabled: userObject.stripeEnabled,
			publishMenu: userObject.publishMenu
		})
	}

	addToCart = (newCartItem) => {
		let newCart = this.state.cart
		newCart.push(newCartItem)
		let newSubtotal = 0
		for (var i=0; i < newCart.length; i++) {
			let priceOfItem = newCart[i].item.price
			let qtyOfItem = newCart[i].quantity
			newSubtotal = newSubtotal + priceOfItem*qtyOfItem
		}
		newSubtotal = parseFloat(newSubtotal)
		this.setState({
			cart: newCart,
			subtotal: newSubtotal,
			tax: newSubtotal*0.05
		})
	}

	render() {
		let listCategories = this.state.items.map((item, i) => {
			return (
				<div key={i}>
					<div className="mainContainer">
						<div className="mcBody">
							
							<LiveItems category={i} list={item.items} fullState={this.state.items} addToCart={this.addToCart}/>

							<div className="spaceRow"></div>
						</div>
					</div>
				</div>

			)

		})
		return (
			<div>
				<div className="slideshow"></div>
				<div className="content">

					<div className="mainInfoCircle">{this.state.settings.name}</div>

					<div className="centerSubtitle">{this.state.settings.phone} | {this.state.settings.address} | {this.state.settings.city}, {this.state.settings.state}</div>

					{listCategories}

					<Cart cart={this.state.cart} precart={this.state.precart} subtotal={this.state.subtotal} tax={this.state.tax} deliveryEnabled={this.state.deliveryEnabled} deliveryOptions={this.state.deliveryOptions} userId={this.props.params.restaurantId} allowDelivery={this.state.settings.delivery} stripeEnabled={this.state.stripeEnabled} publishMenu={this.state.publishMenu} />

					<div className="liveFooter"></div>
				</div>
			</div>
		)
	}
}

class LiveItems extends React.Component {
	constructor (props) {
		super(props)
		this.state = {
			items: this.props.fullState,
			// below are for precart
			open: false,
			quantity: 1,
			instructions: '',
			item: {
				title: false,
				desc: false,
				price: false
			}
		}
	}

	preCart = (itemPicked) => {
		this.setState({
			open: true,
			item: itemPicked
		})
	}
	addToCart = () => {
		let newCartItem = {
			item: this.state.item,
			quantity: this.state.quantity,
			instructions: this.state.instructions
		}
		this.props.addToCart(newCartItem)
		this.setState({
			open: false,
			quantity: 1,
			instructions: ''
		})
	}

	quantityChange = (event) => {
		this.setState({
			quantity: event.target.value
		})
	}
	instructionsChange = (event) => {
		this.setState({
			instructions: event.target.value
		})
	}
	preCartCancel = () => {
		this.setState({
			open: false,
			quantity: 1,
			instructions: ''
		})
	}

	render () {
		let listItems = this.props.list.map((item, i) => {
			if (item.type == 'item') {
				return (
					<div key={i}>
						<div className="menuRow">
							<div className="orderColumn">
								<div className="atcCircle" onClick={() => this.preCart(item)}>+</div>
								<div className="atcPrice">$<span className="itemPrice">{item.price}</span></div>
							</div>
							<div className="menuColumn">
								<div className="leftTitle">{item.title}</div>
								<div className="itemDesc">{item.desc}</div>
							</div>
						</div>
					</div>
				)
			} else if (item.type == 'title') {
				return (
					<div key={i}>
						<div className="menuRow">
						  <div className="centerTitle">{item.title}</div>
					  </div>
					</div>
				)
			} else if (item.type == 'subtitle') {
				return (
					<div key={i}>
						<div className="menuRow">
							<div className="orderColumn"></div>
							<div className="menuColumn">
								<div className="menuSubtitle">{item.title}</div>
							</div>
						</div>
					</div>
				)
			}
		})

		return (
			<div>
				{listItems}


				<div className={this.state.open ? 'preCart' : 'preCartHidden'}>
					<div className="preCartInner">
						<div className="preCartTitle">{this.state.item.title}</div>
						<div className="preCartQuantity">
							Qty: <input type="number" value={this.state.quantity} onChange={this.quantityChange}/>
						</div>
						<div className="preCartInstructions">
							<input type="textarea" placeholder="Additional instructions" value={this.state.instructions} onChange={this.instructionsChange}/>
						</div>
						<div className="preCartSubmit" onClick={() => this.addToCart()}>Submit</div>
						<div className="preCartCancel" onClick={() => this.preCartCancel()}>Cancel</div>
					</div>
				</div>
			</div>
		)
	}
}

class StripeToken extends React.Component {
	constructor (props) {
		super(props)
	}
	onToken = (token) => {
		let newCheckoutObject, x, y
		newCheckoutObject = this.props.checkoutObject
		newCheckoutObject.token = token
		if(newCheckoutObject.deliveryEnabled) {
			socket.emit('addressCheck', newCheckoutObject.address, function (error, body) {
				x = JSON.parse(body).results[0].geometry.location.lat
				y = JSON.parse(body).results[0].geometry.location.lng
				var point = {
				  "type": "FeatureCollection",
				  "features": [
				    {
				      "type": "Feature",
				      "properties": {},
				      "geometry": {
				        "type": "Point",
				        "coordinates": [y, x]
				      }
				   }]
				}
				console.log(point)
				var searchPolygon = {
				  "type": "FeatureCollection",
				  "features": [
				    {
				      "type": "Feature",
				      "properties": {},
				      "geometry": {
				        "type": "Polygon",
				        "coordinates": [newCheckoutObject.deliveryOptions.polygon]
				      }
				    }
				  ]
				}
				console.log(searchPolygon)
				var withinPolygon = turf.within(point, searchPolygon)
				console.log(withinPolygon)
				if(withinPolygon.features.length > 0) {
					socket.emit('mongoNewOrders', newCheckoutObject.userId, newCheckoutObject)
				} else {
					alert('The given address is not in the delivery zone')
				}
			})
		} else {
			socket.emit('mongoNewOrders', newCheckoutObject.userId, newCheckoutObject)
		}
	}

	render() {

		let addedUp = parseFloat(this.props.checkoutObject.subtotal) + parseFloat(this.props.checkoutObject.tax) + parseFloat(this.props.checkoutObject.tip) + parseFloat(this.props.checkoutObject.deliveryFeeCharged)
		return (
			<StripeCheckout
				token={this.onToken}
				stripeKey='pk_test_PcqMa5DZb6TMxoQkNrgbVK32'
				amount={addedUp * 100}
				currency='USD'
				email='pachalabs@gmail.com'
			/>
		)
	}
}

class Cart extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			precart: this.props.precart,
			cart: this.props.cart,
			tip: 0,
			expanded: false,
			deliveryEnabled: false,
			deliveryFeeCharged: 0,
			address: ''
		}
	}

	handleClick = () => {
		this.setState({
			expanded: !this.state.expanded
		})
	}

	addTip = (event) => {
		let tipString = event.target.value
		tipString = parseFloat(tipString)
		this.setState({
			tip: tipString
		})
	}
	handleDelivery = (event) => {
		let deliveryFeeSwitch = event.target.value == 'true' ? this.props.deliveryOptions.deliveryFee : 0
		console.log(event.target, deliveryFeeSwitch)
		this.setState({
			deliveryEnabled: event.target.value,
			deliveryFeeCharged: deliveryFeeSwitch
		})
	}
	handleAddress = (event) => {
		this.setState({
			address: event.target.value
		})
	}
	render() {
		let cartItemList = this.state.cart.map((item, i) => {
			return (
				<div key={i} className="cartLine cartUnderline">
					<div className="lineTitle">{item.quantity} x ${item.item.price} : {item.item.title}</div>
					<div className="lineInstructions">{item.instructions}</div>
				</div>
			)
		})

		let rightNow = moment().format('MMMM Do YYYY, h:mm:ss a')
		let checkoutObject = {
			cart: this.props.cart,
			subtotal: this.props.subtotal,
			deliveryEnabled: this.state.deliveryEnabled,
			address: this.state.address,
			deliveryOptions: this.props.deliveryOptions,
			deliveryFeeCharged: this.state.deliveryFeeCharged,
			tax: this.props.tax,
			tip: this.state.tip,
			userId: this.props.userId,
			date: rightNow,
			actionTaken: false,
			stripeEnabled: this.props.stripeEnabled,
			deliveryFeeCharged: this.state.deliveryFeeCharged,
			total: isNaN(parseFloat(this.state.tip)) ? (parseFloat(this.props.subtotal) + parseFloat(this.props.tax)).toFixed(2) : (parseFloat(this.props.subtotal) + parseFloat(this.props.tax) + parseFloat(this.state.tip) + parseFloat(this.state.deliveryFeeCharged)).toFixed(2)
		}
		console.log(this.props.stripeEnabled)
		console.log(this.props.publishMenu)
		return (
			<div className="cartAll">
				<div className="cartTitle" onClick={() => this.handleClick()}>
					Cart : ${isNaN(parseFloat(this.state.tip)) ? (parseFloat(this.props.subtotal) + parseFloat(this.props.tax)).toFixed(2) : (parseFloat(this.props.subtotal) + parseFloat(this.props.tax) + parseFloat(this.state.tip) + parseFloat(this.state.deliveryFeeCharged)).toFixed(2)}
				</div>
				<div className={this.state.expanded ? 'cartExpanded' : 'cartHidden'}>
					{cartItemList}

					<div className="cartLine">Subtotal: ${this.props.subtotal.toFixed(2)}</div>
					<div className={this.props.allowDelivery && this.props.deliveryOptions.polygon.length > 0 ? "cartLine" : "cartLine hidden"}>Delivery: 
						<select value={this.state.deliveryEnabled} onChange={this.handleDelivery}>
							<option value={false}>No</option>
							<option value={true}>Yes</option>
						</select>
					</div>
					<div className={this.state.deliveryEnabled ? "cartLine" : "cartLine hidden"} onChange={this.handleAddress} placeholder="Type your full address">Address: <input type="text"/></div>
					<div className={this.state.deliveryEnabled ? "cartLine" : "cartLine hidden"}>Delivery Fee: ${this.props.deliveryOptions.deliveryFee}</div>
					<div className={this.state.deliveryEnabled ? "cartLine" : "cartLine hidden"}>Delivery Min: ${this.props.deliveryOptions.deliveryMin}</div>
					<div className="cartLine">Tax: ${this.props.tax.toFixed(2)}</div>
					<div className="cartLine">Tip: $<input type="number" value={this.state.tip} onChange={this.addTip}/></div>
					<div className="cartLine">Total: ${isNaN(parseFloat(this.state.tip)) ? (parseFloat(this.props.subtotal) + parseFloat(this.props.tax)).toFixed(2) : (parseFloat(this.props.subtotal) + parseFloat(this.props.tax) + parseFloat(this.state.tip) + parseFloat(this.state.deliveryFeeCharged)).toFixed(2)}</div>

					<div className={this.props.stripeEnabled && this.props.publishMenu ? "cartLine" : "cartLine hidden"}>
						<StripeToken
							checkoutObject={checkoutObject}
						/>
					</div>
					<div className={!this.props.stripeEnabled || !this.props.publishMenu ? "cartLine" : "cartLine hidden"}>
						To enable payments connect with Stripe and publish menu on the settings page
					</div>
				</div>
			</div>
		)
	}
}

export default LiveMenu