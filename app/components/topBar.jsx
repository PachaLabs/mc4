import React from 'react'
import logo from './logo.svg'
import { LogoutLink } from 'react-stormpath';

class TopBar extends React.Component {
	static contextTypes = {
		user: React.PropTypes.object
	}
	render() {
		console.log(this.context.user.customData.userId)
		let restaurantPath = "/#/menu/" + this.context.user.customData.userId
		return (
			<div>
				<div className="mainContainerBorderless topBar">
				    <img className="logoPic" src={logo}/>
				    <a className="logoText" href="/">menucamp</a>
				    <div className="topRight">
					    <LogoutLink className="accountButton">Logout</LogoutLink>
					</div>
				</div>

				<div className="dashBar"></div>
				<div className="dashMenu">
					<div className="dashMenuInner">
						<a className="dashMenuItem" href="/#/dashboard">Dashboard</a>
						<a className="dashMenuItem" href="/#/settings">Settings</a>
						<a className="dashMenuItem" href="/#/builder">Builder</a>
						<a className="dashMenuItem" href={restaurantPath}>Live Menu</a>
					</div>
				</div>
			</div>
		)
	}
}

export default TopBar