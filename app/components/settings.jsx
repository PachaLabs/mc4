import React from 'react'
import TopBar from './topBar.jsx'

import io from 'socket.io-client'
var socket = io()

class SingleToggle extends React.Component {	
	static contextTypes = {
		user: React.PropTypes.object
	}

	constructor(props) {
		super(props)
		let customField = {}
		customField[this.props.dataName] = this.props.currentState
		this.state = customField
		console.log('toggle', this.state)
	}
	componentDidMount() {
		socket.emit('requestData', this.context.user, (err, userObject) => {
			this.firstLoad(userObject)
		})
	}
	firstLoad = (userObject) => {
		let customField = {}
		console.log(userObject)
		customField[this.props.dataName] = userObject.settings[this.props.dataName]
		this.setState(customField, function() {
			console.log('firstLoad', this.state)
		})

	}
	handleClick = () => {
		let customField = {}
		customField[this.props.dataName] = !this.state[this.props.dataName]
		this.setState(customField, () => {
			console.log (this.props.dataName,this.state[this.props.dataName])
			this.props.alterState(this.state[this.props.dataName])
			console.log('toggleClick', this.state)
		})

	}

	render () {
		return (
			<div className="settingsRow">
				<div className="switchColumn">
					<div className="toggleOuter" onClick={() => this.handleClick()}>
						<div className={this.state[this.props.dataName] ? 'toggleInner': 'toggleInner offSwitch'}></div>
					</div>
					<div className="toggleLabel">{this.state[this.props.dataName] ? 'ON': 'OFF'}</div>
				</div>
				<div className="descColumn">
					<div className="leftTitle">{this.props.title}</div>
					<div>{this.props.description}</div>
				</div>
			</div>
		)
	}
}

class StoreHours extends React.Component {
	static contextTypes = {
		user: React.PropTypes.object
	}
	constructor(props) {
		super(props)
		this.state = {
			hours: this.props.currentState
		}
	}
	componentDidMount() {
		socket.emit('requestData', this.context.user, (err, userObject) => {
			this.firstLoad(userObject)
			console.log(userObject)
		})
	}
	firstLoad = (userObject) => {
		let customField = {
			hours: userObject.settings.hours
		}
		this.setState(customField, function() {
			console.log('firstLoadHours', this.state)
		})

	}
	hoursMongo = () => {
		console.log(this.context.user)
		console.log(this.state.hours)
		socket.emit('mongoUpdate', this.context.user, 'settings.hours', this.state.hours)
	}
	updateStoreHours = (newState) => {
		let targetDay = newState.day
		let newHoursArray = this.state.hours
		for (var i=0; i < this.state.hours.length; i++) {
			if(this.state.hours[i].day == targetDay) {
				newHoursArray[i] = {
					day: targetDay,
					hoursAm: newState.hoursAm,
					hoursPm: newState.hoursPm
				}

			}
		}
		this.setState({
			hours: newHoursArray
		}, () => {
			console.log(this.state)
		})
	}
	render () {
		let dailyHoursList = this.state.hours.map((item, i) => {
			return (
				<DailyHours key={i} day={item.day} hoursAm={item.hoursAm} hoursPm={item.hoursPm} updateStoreHours={this.updateStoreHours}/>
			)
		})
		return (
			<div>
				<div className="settingsRow">
					<div className="switchColumn">
					</div>
					<div className="descColumn">
						<div className="leftTitle">Daily Hours</div>
					</div>
				</div>
				
				{dailyHoursList}
				<div className="settingsRow">
					<div className="switchColumn">
						<div onClick={this.hoursMongo} className="blueButton">Save</div>
					</div>
				</div>
				<div className="spaceRow"></div>
			</div>
		)
	}
}

class DailyHours extends React.Component {
	constructor(props) {
		super(props)
		console.log('daily hours props')
		console.log(props)
		this.state = {
			day: this.props.day,
			hoursAm: this.props.hoursAm,
			hoursPm: this.props.hoursPm
		}
	}
	updateHoursAm = (event) => {
		this.setState({
			hoursAm: event.target.value
		}, function () {
			this.props.updateStoreHours(this.state)
			console.log(this.state)
		})
	}
	updateHoursPm = (event) => {
		this.setState({
			hoursPm: event.target.value
		}, function	() {
			this.props.updateStoreHours(this.state)
		})
	}
	render () {
		return (
			<div className="settingsRow">
				<div className="switchColumn">
					<div className="toggleLabel">{this.props.day.substring(0,3)}</div>
				</div>
				<div className="descColumn">
					<input type="time" value={this.props.hoursAm} onChange={this.updateHoursAm}/> - 
					<input type="time" value={this.props.hoursPm} onChange={this.updateHoursPm} />
					<span className="nowrap"><input type="checkbox"/> Closed All Day</span>
				</div>
			</div>
		)
	}
}

class StoreInfo extends React.Component {
	handleChange = (e) => {
		this.props.alterGenInfo(this.props.field, e.target.value)
	}

	render () {
		let theField = this.props.field
		console.log(this.props, theField, this.props[theField])
		return (
			<div className="settingsRow">
				<div className="switchColumn">
					<div className="toggleLabel">{this.props.displayLabel}</div>
				</div>
				<div className="descColumn">
					<input className="basicInput" type="text" value={this.props[theField]} onChange={this.handleChange}/>
				</div>
			</div>
		)
	}
}

class MapBox extends React.Component {
	static contextTypes = {
		user: React.PropTypes.object
	}
	constructor(props) {
		super(props)
		console.log(this.props)
	}
	componentDidMount () {
		let center
		let zoom
		let centerPoint
		socket.emit('requestData', this.context.user, (err, userObject) => {
			if (userObject.settings.deliveryOptions.polygon.length > 0) {
				console.log(center)
				zoom = 10
				userObject.settings.deliveryOptions.polygon.reduce(function (x,y) {
			        centerPoint = [x[0] + y[0]/userObject.settings.deliveryOptions.polygon.length, x[1] + y[1]/userObject.settings.deliveryOptions.polygon.length] 
			        return centerPoint
			    }, [0,0])
			    center = centerPoint
			} else {
				center = [-95, 40];
				zoom = 3
			}

			mapboxgl.accessToken = 'pk.eyJ1IjoicGFjaGFsYWJzIiwiYSI6ImNpd2ZrNXNreTAwOHEyb3A5NmdlaDl1YmsifQ.wjtqQ3Jn0MvDmR0eTZvNZw'
			var map = new mapboxgl.Map({
				container: 'map',
				style: 'mapbox://styles/mapbox/streets-v9',
				zoom: zoom,
				center: center
			});
			
			var draw = new MapboxDraw({
			    displayControlsDefault: false,
			    controls: {
			        polygon: true,
			        trash: true
			    }
			});
			map.addControl(draw)

			map.on('load', function () {
				if (userObject.settings.deliveryOptions.polygon.length > 0) {
					var feature = { type: 'Polygon', coordinates: [userObject.settings.deliveryOptions.polygon] };
					var featureId = draw.add(feature);
				}
			})

			var calcButton = document.getElementById('calculate')
			calcButton.onclick = () => {
			    var data = draw.getAll();
			    console.log(data)
			    console.log(data.features.length)
			    console.log(data.features[0])
			    if (data.features.length > 0) {
				    this.mapButtonClick(data.features[0].geometry.coordinates[0])
			    } else {
			    	this.mapButtonClick([])
			    }
			};
		})
	}
    mapButtonClick (theZone) {
        socket.emit('mongoUpdate', this.context.user, 'settings.deliveryOptions.polygon', theZone)
    }
	render () {
		return (
			<div>
				<div id="map"></div>
				<div className='calculation-box'>
				    <p>Draw a polygon using the draw tools.</p>
				    <div id='calculate' className='blueButton'>Save</div>
				    <div id='calculated-area'></div>
				</div>
			</div>
		)
	}
}

class Settings extends React.Component {
	static contextTypes = {
		user: React.PropTypes.object
	}
	constructor(props) {
		super(props)
		this.state = {
  			publish: false,
  			name: '',
  			phone: '',
  			address: '',
  			city: '',
  			state: '',
  			notifications: true,
  			cashPayments: false,
  			deliveryOptions: {
	        	polygon: [],
	            polyPoints: [],
	            deliveryMin: 10,
	            deliveryFee: 0,
	  			deliveryZone: []
  			},
			delivery: false,
  			hours: [
  				{
  					day: 'Monday',
  					hoursAm: '12:00',
  					hoursPm: '10:00'
  				},
  				{
  					day: 'Tuesday',
  					hoursAm: '12:00',
  					hoursPm: '10:00'
  				},
  				{
  					day: 'Wednesday',
  					hoursAm: '10:00',
  					hoursPm: '10:00'
  				},
  				{
  					day: 'Thursday',
  					hoursAm: '10:00',
  					hoursPm: '10:00'
  				},
  				{
  					day: 'Friday',
  					hoursAm: '10:00',
  					hoursPm: '10:00'
  				},
  				{
  					day: 'Saturday',
  					hoursAm: '10:00',
  					hoursPm: '10:00'
  				},
  				{
  					day: 'Sunday',
  					hoursAm: '10:00',
  					hoursPm: '10:00'
  				}
  			]
		}
		console.log('constructor', this.state)
	}

	componentDidMount() {
		socket.emit('requestData', this.context.user, (err, userObject) => {
			this.firstLoad(userObject)
		})
	}
	firstLoad = (userObject) => {
		this.setState(userObject.settings, function() {
			console.log('firstLoad', this.state)
		})

	}

	alterGenInfo = (alter, val) => {
		let newState = {}
		console.log('alter', alter)
		newState[alter] = val
		console.log(newState)
		this.setState(newState)
	}
	saveGenInfo = () => {
		console.log(this.state)
		socket.emit('mongoUpdate', this.context.user, 'settings', this.state)
	}
	alterDeliveryInfo = (alter, val) => {
		let newState = {deliveryOptions: this.state.deliveryOptions}
		console.log('alter', alter)
		newState.deliveryOptions[alter] = val
		console.log(newState)
		this.setState(newState)
	}
	saveDeliveryInfo = () => {
		console.log(this.state)
		socket.emit('mongoUpdate', this.context.user, 'settings.deliveryOptions', this.state.deliveryOptions)
	}

	alterState = (alter, val) => {
		let newState = {}
		newState[alter] = val
		console.log(alter, val)
		alter = 'settings.' + alter
		socket.emit('mongoUpdate', this.context.user, alter, val)
		this.setState(newState, function () {
			console.log(newState)
		})
	}

	render () {
		return (
			<div>
				<TopBar />

				<div className="mainContainer">
					<div className="mcTitle">
						<div className="mcTitleInner">Enable Payments</div>
					</div>
					<div className="mcBody">
						<div className="settingsRow">
							<div className="switchColumn"><a className="blueButton" href="https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_7Hnfaea7DUU8B3jb3cMmHPbwKO8ZyUYD&scope=read_write">Connect</a></div>
							<div className="descColumn">with stripe to enable receiving payments on your menu</div>
						</div>
						<div className="spaceRow"></div>
					</div>
				</div>

				<div className="mainContainer">
					<div className="mcTitle">
						<div className="mcTitleInner">Manage Hours</div>
					</div>
					<div className="mcBody">

						<SingleToggle 
							currentState={this.state.publish} 
							alterState={(val) => this.alterState('publish', val)} 
							title='Publish Menu' 
							description='You must publish your menu to accept online orders. When published, your menu will only accept orders during your specified store hours.'
							dataName='publish' 
						/>
						<StoreHours
							currentState={this.state.hours}

						/>
					</div>
				</div>

				<div className="mainContainer">
					<div className="mcTitle">
						<div className="mcTitleInner">General Information</div>
					</div>
					<div className="mcBody">
						<StoreInfo name={this.state.name} alterGenInfo={(alter, val) => this.alterGenInfo(alter, val)} displayLabel='Name' field='name'/>
						<StoreInfo phone={this.state.phone} alterGenInfo={(alter, val) => this.alterGenInfo(alter, val)} displayLabel='Phone' field='phone'/>
						<StoreInfo address={this.state.address} alterGenInfo={(alter, val) => this.alterGenInfo(alter, val)} displayLabel='Address' field='address'/>
						<StoreInfo city={this.state.city} alterGenInfo={(alter, val) => this.alterGenInfo(alter, val)} displayLabel='City' field='city'/>
						<StoreInfo state={this.state.state} alterGenInfo={(alter, val) => this.alterGenInfo(alter, val)} displayLabel='State' field='state'/>

						<div className="settingsRow">
							<div className="switchColumn">
								<div className="blueButton" onClick={this.saveGenInfo}>Save</div>
							</div>
						</div>
						
						<div className="spaceRow"></div>
					</div>
				</div>

				<div className="mainContainer">
					<div className="mcTitle">
						<div className="mcTitleInner">Delivery Options</div>
					</div>
					<div className="mcBody">
						<SingleToggle 
							currentState={this.state.notifications} 
							alterState={(val) => this.alterState('notifications', val)} 
							title='Notifications' 
							description='Receive an email notification for each order received.'
							dataName='notifications' 
						/>
						<SingleToggle 
							currentState={this.state.allowCash} 
							alterState={(val) => this.alterState('allowCash', val)} 
							title='Cash Payments' 
							description='Enables your customers to indicate they will pay in cash at the time of pick up or delivery.'
							dataName='allowCash' 
						/>
						<SingleToggle 
							currentState={this.state.delivery} 
							alterState={(val) => this.alterState('delivery', val)} 
							title='Delivery Orders' 
							description='Enables your customers to submit delivery orders. Restaurants are responsible for providing delivery services.'
							dataName='delivery' 
						/>

						<DeliveryOptions delivery={this.state.delivery} deliveryMin={this.state.deliveryOptions.deliveryMin} field='deliveryMin' displayLabel='Delivery Minimum' alterDeliveryInfo={(alter, val) => this.alterDeliveryInfo(alter, val)}/>

						<DeliveryOptions delivery={this.state.delivery} deliveryFee={this.state.deliveryOptions.deliveryFee} field='deliveryFee' displayLabel='Delivery Fee' alterDeliveryInfo={(alter, val) => this.alterDeliveryInfo(alter, val)}/>

						<div className={this.state.delivery ? '' : 'hidden'}>
							<div className="settingsRow">
								<div className="switchColumn">
									<div className="blueButton" onClick={this.saveDeliveryInfo}>Save</div>
								</div>
							</div>
						</div>

						<div className="spaceRow"></div>
					</div>
				</div>

				<div className="mainContainer">
					<div className="mcTitle">
						<div className="mcTitleInner">Set Delivery Zone</div>
					</div>
					<div className="mcBody">
						<MapBox />
					</div>
				</div>


				<div className="footer"></div>

			</div>
		)
	}
}




class DeliveryOptions extends React.Component {
	handleChange = (e) => {
		this.props.alterDeliveryInfo(this.props.field, e.target.value)
	}

	render() {
		return (
			<div className={this.props.delivery ? '' : 'hidden'}>
				<div className="settingsRow">
					<div className="switchColumn">
						$<input className="numInput" type="number" onChange={this.handleChange} value={this.props[this.props.field]}/> 
					</div>
					<div className="descColumn">
						<div className="leftTitle">{this.props.displayLabel}</div>
					</div>
				</div>


			</div>
		)
	}
}

export default Settings