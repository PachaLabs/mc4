import React from 'react'
import ReactDOM from 'react-dom'
import { Route, hashHistory } from 'react-router'

import Home from './components/home.jsx'
import HomeBar from './components/homeBar.jsx'
import LiveMenu from './components/liveMenu.jsx'
import Dashboard from './components/dashboard.jsx'
import Builder from './components/builder.jsx'
import Settings from './components/settings.jsx'

import stylesheet from './style.scss'
import ReactStormpath , { LoginForm, LoginRoute, Router, RegistrationForm, AuthenticatedRoute } from 'react-stormpath';

import io from 'socket.io-client'
var socket = io()

ReactStormpath.init();

class Login extends React.Component {
	render () {
		return (
			<div>
				<HomeBar />
				<div className="mainContainer">
					<div className="mcTitle">Login</div>
					<div className="mcBody loginOuter">
						<LoginForm className="loginInner" redirectTo='/dashboard'/>
					</div>
				</div>
			</div>
		)
	}
}

class Register extends React.Component {
	render () {
		return (
			<div>
				<HomeBar />
				<div className="mainContainer">
					<div className="mcTitle">Register</div>
					<div className="mcBody loginOuter">
						<RegistrationForm className="loginInner" redirectTo='/dashboard'/>
					</div>
				</div>
			</div>
		)
	}
}

class NotFound extends React.Component {
	render () {
		return <div>404</div>
	}
}

class StripeAuth extends React.Component {
	static contextTypes = {
		user: React.PropTypes.object
	}
	constructor (props) {
		super(props)
		this.state = {
			stripeAuthCode: props.location.query.code
		}
	}
	componentDidMount () {
		socket.emit('stripeToken', this.state.stripeAuthCode, (error, response) => {
			socket.emit('mongoUpdate', this.context.user, 'stripe', JSON.parse(response));
			hashHistory.push('/settings')
		})
	}
	render () {
		return <div>Connecting to Stripe</div>
	}
}

class App extends React.Component {
	render () {
		return (

			<Router history={hashHistory}>
				<Route path='/' component={Home}/>
				<Route path='/menu/:restaurantId' component={LiveMenu}/>
				<LoginRoute path='login' component={Login}/>
				<Route path='register' component={Register}/>
				<AuthenticatedRoute path='dashboard' component={Dashboard}/>
				<AuthenticatedRoute path='builder' component={Builder}/>
				<AuthenticatedRoute path='settings' component={Settings}/>
				<AuthenticatedRoute path='stripeauth' component={StripeAuth}/>
				<Route path='stripetoken'/>
				<Route path='*' component={NotFound}/>
			</Router>
		)
	}
}

ReactDOM.render(<App/>, document.getElementById('root'))